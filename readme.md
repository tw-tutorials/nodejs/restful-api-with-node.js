# Building a RESTful API with Node.js
Tutorial: [YouTube](https://www.youtube.com/watch?v=0oXYLzuucwE&t=0s&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q&index=2)
---
## 1. What is a RESTful API?
- `API`: **A**pplication **P**rogramming **I**nterface
- `REST`: **Re**preseontational **S**tate **T**ransfer
- Stateless backend
- Different endpoints
- Multiple request types for each endpoint
- Typically JSON based

#### Restful Constraints
- `Client-Server Architecture`: Separation of concerns RESTful API should not care about the UI
- `Stateless`: No Client-Context (e.g. Session) is stored on the server
- `Cacheability`: Responses must define themselves as cacheable or non-cachable
- `Layered System`: Intermediate servers maybe used without the client knowing about it
- `Uniform Interface`: Resources are identified in requests, transferred data is decoupled from db schema; Self descriptive messages, Links to further resources
- `(Code on Demand)`: Executable Code could be transferred
---
## 2. Planning & First Steps
- Our API
  - `/products`
    - GET: Get all products
    - *POST: Add new products*
  - `/products/{id}`
    - GET: Get information about product
    - *PATCH: Change/Update product*
    - *DELETE: Delete product*
  - `/orders`
    - *GET: Get list of all orders*
    - *POST: Add new order*
  - `/orders/{id}`
    - *GET: Get order*
    - *DELETE: Delete order*

*Italic routes are protected and only accessible for authorized users*

1. Create project folder `/node-rest-shop`
2. Initialize npm 
   ```bash
   npm init
   ```
3. Install dependencies
   ```bash
   npm install --save express
   ```
4. Start server
   ```bash
   node server.js
   ```
---
## 3. Adding More Routes to the API
## 4. Handling Errors & Improving the Project Setup
## 5. Parsing the Body & Handling CORS
